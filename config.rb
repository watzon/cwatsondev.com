# Activate and configure extensions
# https://middlemanapp.com/advanced/configuration/#configuring-extensions

activate :autoprefixer do |prefix|
  prefix.browsers = "last 2 versions"
end

# activate :deploy do |deploy|
#   deploy.deploy_method = :git
#   # Optional Settings
#   # deploy.remote   = 'custom-remote' # remote name or git url, default: origin
#   deploy.branch   = 'master' # default: gh-pages
#   # deploy.strategy = :submodule      # commit strategy: can be :force_push or :submodule, default: :force_push
#   # deploy.commit_message = 'custom-message'      # commit message (can be empty), default: Automated commit at `timestamp` by middleman-deploy `version`
# end

activate :directory_indexes
activate :alias

# Layouts
# https://middlemanapp.com/basics/layouts/

# Per-page layout changes
page '/*.xml', layout: false
page '/*.json', layout: false
page '/*.txt', layout: false

page "/.well-known/acme-challenge/wT5vgTnLB8EcagLIqHbj0W4FzZriYmW6vS9QJ3afLCE.html", :directory_index => false

# With alternative layout
# page '/path/to/file.html', layout: 'other_layout'

configure :development do
  # activate :livereload # Reload the browser automatically whenever files change
  set :base_url, "" # Setting empty baseurl
end


configure :build do
  activate :minify_css
  activate :minify_javascript

  set :build_dir, 'public'
  set :base_url, "/cwatsondev.com"

  activate :relative_assets
end

####
# More Options
####

# With alternative layout
# page "/path/to/file.html", layout: :otherlayout

# Proxy pages (http://middlemanapp.com/basics/dynamic-pages/)
# proxy "/this-page-has-no-template.html", "/template-file.html", locals: {
#  which_fake_page: "Rendering a fake page with a local variable" }

###
# Helpers
###

# Methods defined in the helpers block are available in templates
# helpers do
#   def some_helper
#     "Helping"
#   end
# end

# Redirects
# redirect "something/index.html", to: "/something/somethingelse"

# Setting custom directories for css, js and images
# set :css_dir, 'assets/css'
# set :js_dir, 'assets/js'
# set :images_dir, 'assets/img'
